# Tezos-Contract-Quickstart

This is a step-by-step method for setting up your first Smart Contract on the Tezos Alphanet. 


- http://tezos.gitlab.io/mainnet/introduction/howtoget.html
  * wget testnet ..

(before running script)
- install brew
- install docker (with_out_ brew)
  (brew doesn't install docker desktop)

```
export TEZOS_ALPHANET_DO_NOT_PULL=yes
./alphanet.sh start
```

At this point, the alphanet docker images should be running, verify with: `docker ps`

You should see something like:

```bash
$ docker ps
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                              NAMES
bcf1536a35d0        tezos/tezos:alphanet   "/usr/local/bin/entr…"   2 minutes ago       Up About a minute                                      alphanet_endorser-004-Pt24m4xi-test_1
c1fea402f5f8        tezos/tezos:alphanet   "/usr/local/bin/entr…"   2 minutes ago       Up About a minute                                      alphanet_accuser-004-Pt24m4xi-test_1
fd4917af6285        tezos/tezos:alphanet   "/usr/local/bin/entr…"   2 minutes ago       Up About a minute                                      alphanet_accuser-004-Pt24m4xi_1
872ecefaba11        tezos/tezos:alphanet   "/usr/local/bin/entr…"   2 minutes ago       Up About a minute                                      alphanet_baker-004-Pt24m4xi_1
0ce6679d2996        tezos/tezos:alphanet   "/usr/local/bin/entr…"   2 minutes ago       Up About a minute                                      alphanet_endorser-004-Pt24m4xi_1
b39c8f9a0a4f        tezos/tezos:alphanet   "/usr/local/bin/entr…"   2 minutes ago       Up About a minute                                      alphanet_baker-004-Pt24m4xi-test_1
e59c95155d7e        tezos/tezos:alphanet   "/usr/local/bin/entr…"   2 minutes ago       Up 2 minutes        8732/tcp, 0.0.0.0:9732->9732/tcp   alphanet_node_1
```

Then, setting up using: https://stove-labs.github.io/granary/docs/tutorials-deploying-a-tezos-smart-contract-to-alphanet
- Get account from [faucet](https://faucet.tzalpha.net/)
- Move `.json` file to folder where activating account

```bash
cp ~/Downloads/tz1MhGthgRDEK4J5VVzt7r9sBS8FE462DAtr.json .
```


For the following, to run "client" commands on `rpcalpha.tzbeta.net`:
```bash
client do_foo_bar
```

becomes

```bash
./alphanet.sh client -A rpcalpha.tzbeta.net -P 443 -S do_foo_bar
```

To activate the account:
- `client activate account alice with "container:/Users/tocquevillegroup/tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9.json"`
  * NOTE: returns empty `Error`

  * The `container:` prefix doesn't appear to be working properly. A workaround is to use:

```bash
$ client activate account alice with "container:tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9.json"
$ client activate account alice with "/tmp/tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9.json"
```


To check for the balance:

```bash
$ client get balance for alice
Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

26963.992755 ꜩ
```


Upload the contract to the alphanet:

```bash
$ client originate contract int_storage for alice transferring 0 from alice running "$(cat int_storage.tz)" --init 0 --dry-run
>>>Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

Waiting for the node to be bootstrapped before injection...
Current head: BLtuZU8VUguf (timestamp: 2019-08-05T18:46:16-00:00, validation: 2019-08-05T18:46:38-00:00)
Node is bootstrapped, ready for injecting operations.
Fatal error:
  The operation will burn ꜩ0.295 which is higher than the configured burn cap (ꜩ0).
   Use `--burn-cap 0.295` to emit this operation.
```


After dry run, set the burn-cap as described above:

```bash
$ client originate contract int_storage for alice transferring 0 from alice running "$(cat int_storage.tz)" --init 0 --dry-run --burn-cap 0.295
>>>Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

Waiting for the node to be bootstrapped before injection...
Current head: BKkPiXjn2p8X (timestamp: 2019-08-05T18:46:46-00:00, validation: 2019-08-05T18:47:07-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 11190 units (will add 100 for safety)
Estimated storage: 295 bytes added (will add 20 for safety)
Operation: 0x04ca8305f1ec67353aab6c3ec93838b7eb0f0b1e061f3b66f25410ee78e49bee070000319b3c7e1b732c876a8e21dc4693a6edd362327fec09fbea18904e0000ddd678cff86e9ba9e2bac13be30db08ff3c2f629121cd006f18a53b84cf81fb6090000319b3c7e1b732c876a8e21dc4693a6edd362327fad0afcea189a58bb0200319b3c7e1b732c876a8e21dc4693a6edd362327f00000000ff0000001c02000000170500035b0501035b050202000000080316053d036d034200000002000013ca594e5be729e4891c61d47012842280f2010dc5e7bf17c529c06a05043a2567220eb3c9c1f0f2d1bb662337d96636cf405d7d2859ceec52abd75acc174005
Operation hash is 'oovND6REyHxbYVDDbWPzVVxEyhL6uZRn5kKshQm3eMhLrB32Mzy'
Simulation result:
  Manager signed operations:
    From: tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9
    Fee to the baker: ꜩ0.00126
    Expected counter: 406907
    Gas limit: 10000
    Storage limit: 0 bytes
    Balance updates:
      tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9 ............. -ꜩ0.00126
      fees(tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU,269) ... +ꜩ0.00126
    Revelation of manager public key:
      Contract: tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9
      Key: edpkvKvV6getrxpmLWtzUszWCgQCFoqYBxhQ7chsHy6ptZKFTcQkUG
      This revelation was successfully applied
      Consumed gas: 10000
  Manager signed operations:
    From: tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9
    Fee to the baker: ꜩ0.001325
    Expected counter: 406908
    Gas limit: 11290
    Storage limit: 315 bytes
    Balance updates:
      tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9 ............. -ꜩ0.001325
      fees(tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU,269) ... +ꜩ0.001325
    Origination:
      From: tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9
      For: tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9
      Credit: ꜩ0
      Script:
        { parameter int ; storage int ; code { CAR ; NIL operation ; PAIR } }
        Initial storage: 0
        No delegate for this contract
        This origination was successfully applied
        Originated contracts:
          KT1UuvkzgEhbyEEp4NfQNdKmD5nzW4oKk3pD
        Storage size: 38 bytes
        Paid storage size diff: 38 bytes
        Consumed gas: 11190
        Balance updates:
          tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9 ... -ꜩ0.038
          tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9 ... -ꜩ0.257
```

**TODO: We need input/output for this not being a dry run, and to show how to get the contract info from the output and view it here: https://alphanet.tzscan.io/**



To change the stored value to '7':

```bash
$ client transfer 0 from alice to KT1N7KUURJwiT92mz1xY55rFaZouXrhUr778 --arg '7'
>>>Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

Waiting for the node to be bootstrapped before injection...
Current head: BMXhyPwgCYDq (timestamp: 2019-08-05T18:58:36-00:00, validation: 2019-08-05T18:59:05-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 11499 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'ooJHds1qBWMXvgDFavkN4appJ9c6PxuPhXxyhPx6GaxhEvSW7Hb'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

(The "Fatal error" may be ignored in this case.)



Clone [`tzip`](https://gitlab.com/serokell/morley/tzip/),
  check out the `gromak/tm274-update-token-standards` branch,
  and look at [`FA1.2`](https://gitlab.com/serokell/morley/tzip/blob/gromak/tm274-update-token-standards/assets/FA1.2).
  There is a `.md` markdown document that describes the parameters and a source code `.tz` document.
* TODO: these links are in a branch right now; when merged, update links

When originating our `FA1.2` contract, we need to set an initial storage value, described [here](https://gitlab.com/serokell/morley/tzip/blob/master/assets/FA1.2/ManagedLedger.tz#L15)

[This is the Haskell definition of storage](https://gitlab.com/morley-framework/morley/blob/e4915c5b7d4e0dfea19ad5044ff3ea63ffbeb4cc/lorentz-contracts/src/Lorentz/Contracts/ManagedLedger/Athens.hs#L46)

We need to translate that into something Michelson understands... blah:
  [Michelson full grammar](https://tezos.gitlab.io/mainnet/whitedoc/michelson.html#full-grammar)

Open/closed braces `{}` are initial account balances inside contract; address is our alice wallet hash; bool (False) is contract-is-active; 12 is total number of coins (chosen arbitrarily); Left [Address] means proxy is not set

```
--init "Pair {} (Pair (Pair \"tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9\" False) (Pair 12 (Left \"tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9\")))"
```


Dry run contract origination with initial storage value:

```bash
$ client originate contract ManagedLedgerAthens for alice transferring 0 from alice running "$(cat ManagedLedgerAthens.tz)" --dry-run --burn-cap 5.499 --init "Pair {} (Pair (Pair \"tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9\" False) (Pair 12 (Left \"tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9\")))"
>>>Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

Waiting for the node to be bootstrapped before injection...
Current head: BMKY9CSeLadN (timestamp: 2019-08-05T21:00:36-00:00, validation: 2019-08-05T21:02:19-00:00)
Node is bootstrapped, ready for injecting operations.
Fatal error:
  The operation will burn ꜩ9.187 which is higher than the configured burn cap (ꜩ5.499).
   Use `--burn-cap 9.187` to emit this operation.
```

Add burn cap as specified:

```bash
$ client originate contract ManagedLedgerAthens for alice transferring 0 from alice running "$(cat ManagedLedgerAthens.tz)" --dry-run --burn-cap 9.187 --init "Pair {} (Pair (Pair \"tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9\" False) (Pair 12 (Left \"tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9\")))"
>>> Warning:

                This is NOT the Tezos Mainnet.

    The node you are connecting to claims to be running on the
            Tezos Alphanet DEVELOPMENT NETWORK.
        Do NOT use your fundraiser keys on this network.
        Alphanet is a testing network, with free tokens.

Waiting for the node to be bootstrapped before injection...
Current head: BMKY9CSeLadN (timestamp: 2019-08-05T21:00:36-00:00, validation: 2019-08-05T21:02:42-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 242133 units (will add 100 for safety)
Estimated storage: 9187 bytes added (will add 20 for safety)

..... 

Initial storage:
    (Pair {}
        (Pair (Pair "tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9" False)
                (Pair 12 (Left "tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9"))))
No delegate for this contract
This origination was successfully applied
Originated contracts:
    KT1WveBbHYHaudnhacDokUVxwxuV4rJSxYNe
Storage size: 8930 bytes
Paid storage size diff: 8930 bytes
Consumed gas: 242133
Balance updates:
    tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9 ... -ꜩ8.93
    tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9 ... -ꜩ0.257
```

Run live (without --dry-run arg):

```bash
$ client originate contract ManagedLedgerAthens for alice transferring 0 from alice running "$(cat ManagedLedgerAthens.tz)"  --burn-cap 9.187 --init "Pair {} (Pair (Pair \"tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9\" False) (Pair 12 (Left \"tz1QAKmXTscbcs88wE69Pih77AmLsgxF2RL9\")))"

>> Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

Waiting for the node to be bootstrapped before injection...
Current head: BMKY9CSeLadN (timestamp: 2019-08-05T21:00:36-00:00, validation: 2019-08-05T21:03:27-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 242133 units (will add 100 for safety)
Estimated storage: 9187 bytes added (will add 20 for safety)
Operation successfully injected in the node.
Operation hash is 'ooHnLLNpoWHbuAwpDSGKdNXwE7627czx1UMzb4VzxSvGgGY1NYy'
Waiting for the operation to be included...

Fatal error:
  origination simulation failed
```

(Ignore "Fatal error", in this case, if no other error information is provided.)



GENERAL NOTES:
- `alphanet.sh update_script` removes the `+x` permissions from the script (run at end of script)
- Granary docker setup check command typo'd: `https://stove-labs.github.io/granary/docs/getting-started-install`
-

