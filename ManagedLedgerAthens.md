
# FA1.2.1 Quick Start

## Introduction

FA1.2.1 is a smart contract which implements a ledger that maps
identities to balances. This ledger implements transfers of tokens,
a way to approve spending other accounts' tokens, and methods to
allow interacting with it through a proxy contract.

This is a quick start introduction to using the `ManagedLedgerAthens` implementation of
FA1.2.1 for [`Athens`](https://medium.com/tezos/tezos-athens-379f6a0d5332) from
[Serokell](https://gitlab.com/serokell)'s
[`tzip` repository](https://gitlab.com/serokell/morley/tzip).

- A summary of FA1.2.1 may be found [here](https://gitlab.com/serokell/morley/tzip/blob/gromak/tm274-update-token-standards/A/FA1.2.1.md)

- A guide to the `ManagedLedger` contract may be found [here](https://gitlab.com/serokell/morley/tzip/blob/gromak/tm274-update-token-standards/assets/FA1.2/ManagedLedger.md)


## Installing the client and contract CLI

Install the alphanet client and Lorentz contract CLI:

```bash
brew tap TQTezos/homebrew-tq https://gitlab.com/TQTezos/homebrew-tq.git
brew install tezos
brew install lorentz-contract-param
```

Create an alias for the `alphanet` client:

```bash
alias alpha-client="tezos-client -A rpcalpha.tzbeta.net -P 443 -S"
```

## Create alphanet wallets:

- Go to the [faucet](https://faucet.tzalpha.net/)
- Click `Get alphanet ꜩ`, which will download a `.json` file with a new wallet

Activate the account, passing it the path to the `.json` wallet you just downloaded:
- Here, we're calling the account `alice`

```bash
$ alpha-client activate account alice with "~/Downloads/tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP.json"
Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

Waiting for the node to be bootstrapped before injection...
Current head: BL52YjrSCteP (timestamp: 2019-08-12T15:09:16-00:00, validation: 2019-08-12T15:09:28-00:00)
Node is bootstrapped, ready for injecting operations.
Operation successfully injected in the node.
Operation hash is 'opES9TEa9cazEs1mcc6jcbFAWNFtzLewpCD4evct6oG4T1m2od3'
Waiting for the operation to be included...
Error:
  
```

Note: Since the `This is NOT the Tezos Mainnet.` message is repeated for each
command, it has been removed from the following command responses for brevity.


Ensure that the activation was successful:

```bash
$ alpha-client get balance for alice
5310.946554 ꜩ
```


Make a variable for Alice's account address:

```bash
ALICE_ADDRESS="tz1PCA3H4SAGneuoXuVH1YV9yZeWd1foj2oQ"
```

Repeat for `bob`, `BOB_ADDRESS`, respectively, to create a wallet for Bob.
(We'll need two accounts to perform transfers, etc.)


## Originate a new copy of FA1.2.1

Fetch a copy of the `FA1.2.1` from `gromak`'s
`tm274-update-token-standards` branch on `gitlab.com`:

```bash
FA121_CODE=$(curl \
  https://gitlab.com/serokell/morley/tzip/raw/gromak/tm274-update-token-standards/assets/FA1.2/ManagedLedgerAthens.tz)
echo $FA121_CODE
```

The contract must be initialized with several parameters:

```haskell
Pair
  {}                           -- Empty ledger big_map
  (Pair
     (Pair
       "$ALICE_ADDRESS"        -- Administrator address
       False                   -- Is all activity currently paused? (no)
     )
     (Pair
       12                      -- Total supply of tokens
       (Left "$ALICE_ADDRESS") -- Proxy-setup administrator address
     )
  )
```

To initialize it, run:

```bash
$ alpha-client originate contract ManagedLedgerAthens for $ALICE_ADDRESS \
  transferring 0 from $ALICE_ADDRESS running $FA121_CODE \
  --init "Pair {} (Pair (Pair \"$ALICE_ADDRESS\" False) (Pair 12 (Left \"$ALICE_ADDRESS\")))" --burn-cap 9.187

Waiting for the node to be bootstrapped before injection...
Current head: BMTBZo51Wxxw (timestamp: 2019-08-12T15:11:16-00:00, validation: 2019-08-12T15:11:25-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 242133 units (will add 100 for safety)
Estimated storage: 9187 bytes added (will add 20 for safety)
Operation successfully injected in the node.
Operation hash is 'ooL1McAMxjdiVXsv4GLgxtpXH4y41TdZtFWoGcmC6TkJfVagGxm'
Waiting for the operation to be included...

Fatal error:
  origination simulation failed
```

You can get the resulting contract hash by running the following command:

Note: `Script` contents abbreviated with `..`

```bash
$ alpha-client get receipt for ooL1McAMxjdiVXsv4GLgxtpXH4y41TdZtFWoGcmC6TkJfVagGxm

Operation found in block: BLKVEcSCehMfEWw4z33tGkP1kyivRMLV9PCDFgMgKEruaS5qr1Q (pass: 3, offset: 1)
Manager signed operations:
  From: tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP
  Fee to the baker: ꜩ0.00126
  Expected counter: 565660
  Gas limit: 10000
  Storage limit: 0 bytes
  Balance updates:
    tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP ............. -ꜩ0.00126
    fees(tz3WXYtyDUNL91qfiCJtVUX746QpNv5i5ve5,278) ... +ꜩ0.00126
  Revelation of manager public key:
    Contract: tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP
    Key: edpkuVNN5SQzWoyvTaR24oHq7QHadHJkc9vjMpgm913nocP2BJ3kfX
    This revelation was successfully applied
    Consumed gas: 10000
Manager signed operations:
  From: tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP
  Fee to the baker: ꜩ0.033314
  Expected counter: 565661
  Gas limit: 242233
  Storage limit: 9207 bytes
  Balance updates:
    tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP ............. -ꜩ0.033314
    fees(tz3WXYtyDUNL91qfiCJtVUX746QpNv5i5ve5,278) ... +ꜩ0.033314
  Origination:
    From: tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP
    For: tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP
    Credit: ꜩ0
    Script:
      { parameter
      ..
                               NIL operation ;
                               PAIR } } } } } }
      Initial storage:
        (Pair {}
              (Pair (Pair "tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP" False)
                    (Pair 12 (Left "tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP"))))
      No delegate for this contract
      This origination was successfully applied
      Originated contracts:
        KT1LXEg3neJBfuugHSfKdtTsCCP2YEVMnHzR
      Storage size: 8930 bytes
      Paid storage size diff: 8930 bytes
      Consumed gas: 242133
      Balance updates:
        tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP ... -ꜩ8.93
        tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP ... -ꜩ0.257
```

Note: `Originated contracts:` should only contain a single hash: the hash of our new
`ManagedLedgerAthens` contract.


Make a variable for the contract:

```bash
FA121_ADDRESS="KT1LXEg3neJBfuugHSfKdtTsCCP2YEVMnHzR"
```


## Querying FA1.2.1


Instead of providing contract parameters in raw Michelson, we can use
`lorentz-contract-param` to generate them.

The general invocation is:

```bash
lorentz-contract-param [Contract Name] [Contract Parameter]
```

To ensure it's properly installed, run:

```bash
$ which lorentz-contract-param
/usr/local/bin/lorentz-contract-param
```


#### Views

When a contract parameter has a non-unit return type, e.g. `Get Total Supply`,
which returns the total supply of tokens, the contract needs to be able to
store the result somewhere. In particular, another contract accepting the
return value.

For the following examples, we need contracts accepting `address` and `nat` values:

```bash
$ alpha-client originate contract nat_storage for $ALICE_ADDRESS transferring 0 \
  from $ALICE_ADDRESS running \
  "parameter nat; storage nat; code {CAR; NIL operation; PAIR};" \
  --init 0 --burn-cap 0.295

$ alpha-client originate contract address_storage for $ALICE_ADDRESS transferring 0 \
  from $ALICE_ADDRESS running \
  "parameter address; storage address; code {CAR; NIL operation; PAIR};" \
  --init "\"$ALICE_ADDRESS\"" --burn-cap 0.334
```

Note: you may have to set/increase the burn cap to allow the contract(s) to be originated, e.g.:

```bash
$ alpha-client originate contract nat_storage for $ALICE_ADDRESS transferring 0 \
  from $ALICE_ADDRESS running \
  "parameter nat; storage nat; code {CAR; NIL operation; PAIR};" --init 0 --burn-cap 0.295

Waiting for the node to be bootstrapped before injection...
Current head: BKvVhxsUrqTn (timestamp: 2019-08-12T15:17:16-00:00, validation: 2019-08-12T15:17:18-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 11190 units (will add 100 for safety)
Estimated storage: 295 bytes added (will add 20 for safety)
Operation successfully injected in the node.
Operation hash is 'oomRpok2ZDe9uN1j5oY79x7oWUTqR2N713CrWL2TLEq6ZqY53nH'
Waiting for the operation to be included...

Fatal error:
  origination simulation failed
```


Then, using `alpha-client get receipt for [Operation hash]` for each operation
hash, store the contract addresses in variables:

```bash
NAT_STORAGE_ADDRESS="KT1.."
ADDRESS_STORAGE_ADDRESS="KT1.."
```

Next, you may access the stored values with the following two commands for `nat` and `address`, respectively:

```bash
$ alpha-client get script storage for $NAT_STORAGE_ADDRESS
$ alpha-client get script storage for $ADDRESS_STORAGE_ADDRESS

0
"tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP"
```


### Get Total Supply

To get the total supply of tokens (stored in the `NAT_STORAGE_ADDRESS` contract):

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \                                                                             [NORMAL]
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetTotalSupply (View () \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BLDh3k6h4SH3 (timestamp: 2019-08-12T15:23:16-00:00, validation: 2019-08-12T15:23:41-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 211941 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'ooVB6YyeE8qtNcSQ2rWbFZbozpvvK4qPzkLBd4ELvo1GFdBJPLJ'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

Note that the `NAT_STORAGE_ADDRESS` is given in the `View`.
We may access its stored value using the following command:

```bash
$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

12
```


### Get Administrator

To get the current administrator's address (stored in the `ADDRESS_STORAGE_ADDRESS` contract):

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetAdministrator (View () \"$ADDRESS_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BM2pfn2hkxYj (timestamp: 2019-08-12T15:24:16-00:00, validation: 2019-08-12T15:24:35-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212964 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'opJt3e8oN9RFwFT86uncrBnzkyTxAirdaFDV2nWRk63F1B8zHMp'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

As with `NAT_STORAGE_ADDRESS`, we may access the result
using `get script storage for` and ensure that it matches
Alice's address using `echo $ALICE_ADDRESS`:

```bash
$ alpha-client get script storage for $ADDRESS_STORAGE_ADDRESS

"tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP"

$ echo $ALICE_ADDRESS

tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP
```


### Get Balance

To get the balance of tokens for a particular address (stored in the `NAT_STORAGE_ADDRESS` contract):

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetBalance (View \"$ALICE_ADDRESS\" \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BLeiPnLLJ5FR (timestamp: 2019-08-12T15:25:46-00:00, validation: 2019-08-12T15:26:12-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212314 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'op7sUZAG4rYe9UY4Xpj8kNBa47i1HVCmNLhoK2Ppz4fDsfYVz6R'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can then access the result using:

```bash
$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

0
```


### Get Allowance

To get Bob's allowance to withdraw from Alice's account, where Bob's address is `BOB_ADDRESS`:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetAllowance (View (\"owner\" .! \"$ALICE_ADDRESS\", \"spender\" .! \"$BOB_ADDRESS\") \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BMBhtWdDo4LD (timestamp: 2019-08-12T15:30:16-00:00, validation: 2019-08-12T15:31:11-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212402 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'oorBF2S8MmVYwYL7S2xwMuvTQyDzb4Me7uLvNqRS6ReQP4GQ11o'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can then access the result using:

```bash
$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

0
```


## Interacting with FA1.2.1


### Transfer

From `GetBalance` above, we know that `Alice` doesn't have any tokens.

To transfer tokens from Alice to Bob, we first need to give Alice some tokens.
We can do that by minting `5` tokens and allocating them to Alice
(See the `Mint` subsection below):

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "Mint (\"to\" .! \"$ALICE_ADDRESS\", \"value\" .! 5)")"

Waiting for the node to be bootstrapped before injection...
Current head: BLhLd6knMBpW (timestamp: 2019-08-12T15:34:26-00:00, validation: 2019-08-12T15:34:29-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 200864 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'op6ZpxQFPsQAsKXDVkax6eV6qqPcJayZVSEtKEno9Q4FjmMkoBr'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can then access the result using:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetBalance (View \"$ALICE_ADDRESS\" \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BLDtW4w1U2gs (timestamp: 2019-08-12T15:35:26-00:00, validation: 2019-08-12T15:35:47-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212554 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'oo1U7YQFQbRH8pRPhDRG2wzLccH72NThDV3WemfPfrQCvP4ikNC'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

5
```

To transfer `2` tokens from Alice to Bob:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "Transfer (\"from\" .! \"$ALICE_ADDRESS\", \"to\" .! \"$BOB_ADDRESS\", \"value\" .! 2)")"

Waiting for the node to be bootstrapped before injection...
Current head: BLZ84uA3Lewy (timestamp: 2019-08-12T15:38:56-00:00, validation: 2019-08-12T15:39:22-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 202972 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'opP1rsvtWvbJmt5GWopBaCup6wApXzmni6dasb3cL9zNqw3NLuu'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can then access the result using:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetBalance (View \"$BOB_ADDRESS\" \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BLzAVf8FVCnF (timestamp: 2019-08-12T15:40:26-00:00, validation: 2019-08-12T15:40:33-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212560 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'op86Pn3RZgYw2vaB8hW2wFVXpDRSurG7uvdi4QQpWU9pdpTHeRX'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

2
```


### Approve

For Alice to approve Bob to withdraw up to `3` tokens from her address:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "Approve (\"spender\" .! \"$BOB_ADDRESS\", \"value\" .! 3)")" --burn-cap 0.021

Waiting for the node to be bootstrapped before injection...
Current head: BLXvZEbyzBbg (timestamp: 2019-08-12T15:42:26-00:00, validation: 2019-08-12T15:42:59-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 202360 units (will add 100 for safety)
Estimated storage: 21 bytes added (will add 20 for safety)
Operation successfully injected in the node.
Operation hash is 'oovAUWvA1M8nBzmtjHoGz3Te4sBdXeviuh1MynJjAN6MkC8zRC1'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can then check the resulting allowance using:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetAllowance (View (\"owner\" .! \"$ALICE_ADDRESS\", \"spender\" .! \"$BOB_ADDRESS\") \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BLxRRcdaEPQN (timestamp: 2019-08-12T15:44:26-00:00, validation: 2019-08-12T15:44:47-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 213049 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'op5Nkbzix9U4WufFvuNt6dNAJEYKoSKWJ4DmPvDsLKM5nLgCCk9'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

3
```


### Mint

To mint `5` tokens and allocate them to Alice:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "Mint (\"to\" .! \"$ALICE_ADDRESS\", \"value\" .! 5)")"

Waiting for the node to be bootstrapped before injection...
Current head: BLhLd6knMBpW (timestamp: 2019-08-12T15:34:26-00:00, validation: 2019-08-12T15:34:29-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 200864 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'op6ZpxQFPsQAsKXDVkax6eV6qqPcJayZVSEtKEno9Q4FjmMkoBr'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can then access the resulting balance using:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetBalance (View \"$ALICE_ADDRESS\" \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BLDtW4w1U2gs (timestamp: 2019-08-12T15:35:26-00:00, validation: 2019-08-12T15:35:47-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212554 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'oo1U7YQFQbRH8pRPhDRG2wzLccH72NThDV3WemfPfrQCvP4ikNC'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

5
```


### Burn

If you've been following this guide step-by-step, Bob's account should
have `3` tokens at this point. If it doesn't, add at least `1` using
`Transfer` or `Mint`.

Before burning, we first add `3` tokens to Bob's address:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "Mint (\"to\" .! \"$BOB_ADDRESS\", \"value\" .! 3)")"

Waiting for the node to be bootstrapped before injection...
Current head: BL1X2sZueW5y (timestamp: 2019-08-12T15:52:56-00:00, validation: 2019-08-12T15:52:58-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 200867 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'ooDbLAMcXP73aWpRbvoUt6afVfxFyoFfh6zX4zVFFK976dFJAjE'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can ensure Bob has `3` tokens using:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetBalance (View \"$BOB_ADDRESS\" \"$NAT_STORAGE_ADDRESS\")")"
Waiting for the node to be bootstrapped before injection...
Current head: BLWdf7ChnGUi (timestamp: 2019-08-12T15:53:26-00:00, validation: 2019-08-12T15:53:51-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212554 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'oo9xjmLBhpoqf1LcyKfW33SMKfgSicVZ2h5ES9tnHLN8q4d8jsJ'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

3
```

Finally, we can burn `2` of Bob's tokens using:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "Burn (\"from\" .! \"$BOB_ADDRESS\", \"value\" .! 2)")"

Waiting for the node to be bootstrapped before injection...
Current head: BLXUqWJzzpaE (timestamp: 2019-08-12T15:54:26-00:00, validation: 2019-08-12T15:54:32-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 201155 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'opTu3azsgtvcz24qwp7EpsXiEMJJnTyfC7Q12ZgNctxcBjYiUVc'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetBalance (View \"$BOB_ADDRESS\" \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BMTpJDH3jfZ9 (timestamp: 2019-08-12T15:54:56-00:00, validation: 2019-08-12T15:55:13-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212560 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'ooh2PbHdHp8kKC1RqoV6qtwf23TRYnYgEcMfuxrRRVvN9P6XvhP'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client get script storage for $NAT_STORAGE_ADDRESS

1
```

## Miscellaneous features

### Pause Contract

To pause the contract:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  SetPause True)"

Waiting for the node to be bootstrapped before injection...
Current head: BLv7SgaiYUgX (timestamp: 2019-08-12T16:53:36-00:00, validation: 2019-08-12T16:53:53-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 199251 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'op8yw9kMJZRP1sRJj3CN44zBAv8fH1HyjegrmW5rommweybEPSB'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can check whether the contract is paused using:

```bash
$ alpha-client get script storage for $FA121_ADDRESS

Pair {}
     (Pair (Pair "tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP" True)
           (Pair 19 (Left "tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP")))
```

Note: the `True` value indicates that it's paused,
a `False` value indicicates that it's _not_ paused.

To unpause the contract:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  SetPause False)"

Waiting for the node to be bootstrapped before injection...
Current head: BMCc6zfsVyEU (timestamp: 2019-08-12T16:54:36-00:00, validation: 2019-08-12T16:54:52-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 199251 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'ooS6Pnm3Arw65pDmsWg4E2P8NKcZGsSvKu7AeaHLoG5FoQBsz5k'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client get script storage for $FA121_ADDRESS

Pair {}
     (Pair (Pair "tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP" False)
           (Pair 19 (Left "tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP")))
```


### Set New Administrator

To make Bob the new administrator:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "SetAdministrator \"$BOB_ADDRESS\"")"

Waiting for the node to be bootstrapped before injection...
Current head: BLpjHU7rULfH (timestamp: 2019-08-12T16:56:06-00:00, validation: 2019-08-12T16:56:31-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 199261 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'onnfCRov3CKkhSRx5oqXxFm6gEfwV5HCxrFo8ETpyTLaqNijkaF'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed
```

We can ensure that the administrator has been updated using:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetAdministrator (View () \"$ADDRESS_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BMMXtHb6xXAW (timestamp: 2019-08-12T16:56:36-00:00, validation: 2019-08-12T16:57:05-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212730 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'opGRByecVAs43RJWznaiecg8zaenRENRGM65UbkxbQr3cqPf62t'
Waiting for the operation to be included...

Fatal error:
  transfer simulation failed

$ alpha-client get script storage for $ADDRESS_STORAGE_ADDRESS

"tz1WWkcu7VFWgUTB9AAAomXGMS27tPKXrE9C"

$ echo $BOB_ADDRESS

tz1WWkcu7VFWgUTB9AAAomXGMS27tPKXrE9C
```


### Set New Proxy

To set a new proxy address, where the FA1.2.1 proxy contract address is `PROXY_ADDRESS`:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --dry-run --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "SetProxy \"$PROXY_ADDRESS\"")"
```

You may access the current proxy setting using the following:

```bash
$ alpha-client get script storage for $FA121_ADDRESS

Pair {}
     (Pair (Pair "tz1WWkcu7VFWgUTB9AAAomXGMS27tPKXrE9C" False)
           (Pair 19 (Left "tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP")))
```

Where the storage value may be interpreted as follows:

```bash
Pair {}
     (Pair (Pair "[Administrator Address]" [Is paused])
           (Pair [Total Supply] ([Proxy Setting])))
```

Where the `[Proxy Setting]` may be one of:
- `Left "[Proxy Administrator Address]"`
- `Right "[Proxy Contract Address]"`


## Troubleshooting


### `Counter _ already used`

This error occurs when a dependent operation has not yet been included
and can take one of the following forms:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetBalance (View \"$ALICE_ADDRESS\" \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BMe8VmYBa1Ye (timestamp: 2019-08-12T15:34:56-00:00, validation: 2019-08-12T15:35:00-00:00)
Node is bootstrapped, ready for injecting operations.
Counter 565668 already used for contract tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP (expected 565669)
Fatal error:
  transfer simulation failed
```

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "GetBalance (View \"$BOB_ADDRESS\" \"$NAT_STORAGE_ADDRESS\")")"

Waiting for the node to be bootstrapped before injection...
Current head: BL7QrQjGhkut (timestamp: 2019-08-12T15:39:26-00:00, validation: 2019-08-12T15:39:45-00:00)
Node is bootstrapped, ready for injecting operations.
Estimated gas: 212314 units (will add 100 for safety)
Estimated storage: no bytes added
Unregistred error:
  { "kind": "generic",
    "error":
      "Error while applying operation op1imCEMKFm7bTN9cwX4uoKPb6FjHJDv6NCkWKGVE7srh4XN6UP:\nbranch refused (Error:\n                  Counter 565670 already used for contract tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP (expected 565671)\n)" }
Fatal error:
  transfer simulation failed
```

To fix, either:
- Wait for the operation to be included
- Run `alpha-client wait for [Operation Hash] to be included`
  with the dependent operation's `Operation Hash` before continuing


### `higher than the configured burn cap`

This error may occur when `--burn-cap` is not specified or it's set too low for
the current operation:

```bash
$ alpha-client transfer 0 from $ALICE_ADDRESS to $FA121_ADDRESS --arg \
  "$(lorentz-contract-param ManagedLedgerAthens \
  "Approve (\"spender\" .! \"$BOB_ADDRESS\", \"value\" .! 3)")"
Waiting for the node to be bootstrapped before injection...
Current head: BLBsvLowXAPf (timestamp: 2019-08-12T15:41:56-00:00, validation: 2019-08-12T15:42:45-00:00)
Node is bootstrapped, ready for injecting operations.
Fatal error:
  The operation will burn ꜩ0.021 which is higher than the configured burn cap (ꜩ0).
   Use `--burn-cap 0.021` to emit this operation.
```

To fix, replace or add `--burn-cap` with the given recommendation
(in this case, `--burn-cap 0.021`)

